/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package digitalcamera;

/**
 *
 * @author jaiminlakhani
 */
public class PhoneCamera extends DigitalCamera{
    
    double internalMemorySize;
    
    PhoneCamera(String make, String model, double megapixels, double internalMemorySize){
        super(make, model, megapixels);      
        this.internalMemorySize = internalMemorySize;   
    }
    
    public double getInternalMemorySize() {
        return internalMemorySize;
    }
    
    public String describeCamera(){
        return getMake()+"\n"+getModel()+"\n"+"Megapixels: "+getMegapixels()+"\nInternal Memory Size: "+getInternalMemorySize()+" GB\n";
    }
}
