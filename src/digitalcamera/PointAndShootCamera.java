/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package digitalcamera;

/**
 *
 * @author jaiminlakhani
 */
public class PointAndShootCamera extends DigitalCamera{
    
    double externalMemorySize;
    
    PointAndShootCamera(String make, String model, double megapixels, double externalMemorySize){
        super(make, model, megapixels);
        this.externalMemorySize = externalMemorySize;
    }
    
    public double getExternalMemorySize() {
        return externalMemorySize;
    }
    
    public String describeCamera(){
        return getMake()+"\n"+getModel()+"\n"+"Megapixels: "+getMegapixels()+"\nExternal Memory Size: "+getExternalMemorySize()+" GB\n";
    }
}
