/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package digitalcamera;

/**
 *
 * @author jaiminlakhani
 */
public class DigitalCameraSimulation {
    public static void main(String[] args) {
        PointAndShootCamera psc = new PointAndShootCamera("Canon", "Powershot A590", 8.0, 16);
        PhoneCamera pc = new PhoneCamera("Apple", "iPhone", 6.0, 64);
        
        System.out.println(psc.describeCamera());
        System.out.println(pc.describeCamera());
    }
}
