/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package digitalcamera;

/**
 *
 * @author jaiminlakhani
 */
public abstract class DigitalCamera {
    String make;
    String model;
    double megapixels;
        
    DigitalCamera(String make, String model, double megapixels) {
        this.make = make;
        this.model = model;
        this.megapixels = megapixels;
    }
    
    public String getMake() {
        return make;
    }
    
    public String getModel() {
        return model;
    }
    
    public double getMegapixels() {
        return megapixels;
    }
    
}
